<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.List,java.util.Base64"%><!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' rel='stylesheet'>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
    <title>View</title>
  </head>
  <body class='container'>
    <% if(request.getParameter("user") != null && !"".equals(request.getParameter("user"))) { %>
    <div class='card' style='width:20rem;margin:0 auto;float:none;'>
      <img class='card-img-top' src='https://www.w3schools.com/bootstrap4/img_avatar3.png' style='width:100%'/>
      <div class='card-body'>
        <h4 class='card-title'><%=request.getAttribute("login")%></h4>
        <p class='card-text'>E-mail: <b><%=request.getAttribute("email")%></b><br>Posts: <b><%=request.getAttribute("posts")%></b></p>
      </div>
    <% } else if(request.getParameter("post") != null && !"".equals(request.getParameter("post"))) { %>
    <div class='card'>
      <div class='card-header'><a class='btn btn-sm btn-success' href='/main' style='float:right;'>Back</a><%=new String(Base64.getDecoder().decode(request.getAttribute("header").toString().getBytes("windows-1251")), "utf-8")%></div>
      <div class='card-body'>
        <%=new String(Base64.getDecoder().decode(request.getAttribute("body").toString().getBytes("windows-1251")), "utf-8")%><br>
        <span class='badge badge-info'><b>#<%=new String(Base64.getDecoder().decode(request.getAttribute("tags").toString().getBytes("windows-1251")), "utf-8").replaceAll(" ", " #") %></b></span>
        <span class='badge badge-secondary' style='float:right;'><a style='color:white;' href='/view?user=<%=request.getAttribute("author").toString() %>'><%=request.getAttribute("author").toString() %></a></span>
      </div>
        <div class='card-footer list-group' style='padding:0px;'>
          <div class='list-group-item' style='padding:5px;'>Author: <span class='badge badge-primary'><%=request.getAttribute("me")%> (<%=request.getAttribute("myMail")%>)</span><br><textarea rows='2' class='form-control' id='newComment'></textarea><div class='btn btn-sm btn-success' style='float:right;' onclick='addNewComment();'>Comment</div></div>
          <% List<Object[]> lstComments = (List)request.getAttribute("comments");
        for(Object[] comment : lstComments) { %>
        <div class='list-group-item list-group-item-info' style='padding:5px;'><span class='badge badge-secondary'><a style='color:white;' href='/view?user=<%=comment[0].toString()%>'><%=comment[0].toString()%> (<%=comment[2].toString()%>)</a></span><% if(request.getAttribute("me").toString().equals(request.getAttribute("author").toString())) { %> <div class='badge badge-danger' style='cursor:pointer;' onclick='removeComment(<%=comment[3].toString()%>);'>&times;</div><% } %><br><%=new String(Base64.getDecoder().decode(comment[1].toString().getBytes("windows-1251")), "utf-8")%></div>
        <% } %>
      </div>
      <script>
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-,_",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9-,_]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
        function removeComment(cid) {
          $.ajax({url: '/remove', type: 'GET', data: 'comment=' + cid, success: function() { window.location.href = '/view?post=<%=request.getParameter("post")%>'; }});
        }
        function addNewComment() {
          $.ajax({url: '/addComment', type: 'POST', data: 'post=<%=request.getParameter("post")%>&newComment=' + Base64.encode($('#newComment').val()), success: function() { window.location.href = '/view?post=<%=request.getParameter("post")%>'; }});
        }
      </script>
    <% } %>
    </div>
  </body>
</html>
