<%@page contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' rel='stylesheet'>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
    <title>Login</title>
  </head>
  <body class='container'>
    <form method='post'>
      <div class='card'>
        <div class='card-header text-center'><b>Login</b></div>
        <div class='card-block' style='padding:15px;'>
          <div class='form-group'>
            <label for='username'>Login:</label>
            <input type='text' class='form-control' id='username' aria-describedby='username' name='username' required pattern='[A-Za-z0-9]{3,}' <%=(request.getSession().getAttribute("userName") == null ? "" : "value='" + request.getSession().getAttribute("userName") + "'")%>/>
          </div>
          <div class='form-group'>
            <label for='userpass'>Password:</label>
            <input type='password' class='form-control' id='userpass' aria-describedby='userpass' name='password' required pattern='[A-Za-z0-9-=_+()$#@!~,.]{3,}'/>
          </div>
        </div>
        <div class='card-footer text-right'>
          <input type='submit' class='btn btn-success' value='Login' style='cursor:pointer;'/>
        </div>
      </div>
    </form>
  </body>
</html>