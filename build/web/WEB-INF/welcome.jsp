<%@page contentType="text/html" pageEncoding="UTF-8"%><%request.getSession().setAttribute("userName", "");%><!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' rel='stylesheet'>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
    <title>Welcome</title>
  </head>
  <body class='container' style='background: url("http://lorempixel.com/800/600/people");'>
    <div class='jumbotron' style='text-align:center;'>
      <h1>Welcome to personal blog</h1><br>
      <a href='/sign' class='btn btn-success btn-lg'>Sign up</a>
      <a href='/login' class='btn btn-primary btn-lg'>Login</a>
    </div>
  </body>
</html>