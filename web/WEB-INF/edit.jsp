<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.Base64"%><!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' rel='stylesheet'>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
    <title>Edit post</title>
  </head>
  <body class='container'>
    
    
    <form class='card' action='/edit' method='post'>
      <input type='hidden' name='post' value='<%=request.getParameter("post")%>'/>
      <div class='card-header'><div id='header'><%=new String(Base64.getDecoder().decode(request.getAttribute("header").toString().getBytes("windows-1251")), "utf-8")%></div></div>
      <div class='card-body'>
        <div id='body'><%=new String(Base64.getDecoder().decode(request.getAttribute("body").toString().getBytes("windows-1251")), "utf-8")%></div><br>
        <span class='badge badge-info'><b>#<%=new String(Base64.getDecoder().decode(request.getAttribute("tags").toString().getBytes("windows-1251")), "utf-8").replaceAll(" ", " #") %></b></span>
        <span class='badge badge-secondary' style='float:right;'><a style='color:white;' href='/view?user=<%=request.getAttribute("author").toString() %>'><%=request.getAttribute("author").toString() %></a></span>
      </div>
      <div class='card-footer'><input type='submit' class='btn btn-success' value='Save post' /></div>
    </form>
    
      
    <script>
      $('#header').click(function(){
        var content = $('#header').text();
        if(content !== '') {
        $('#header').html('');
        $('<input></input>')
        .attr({
            'type': 'text',
            'name': 'postHeader',
            'id': 'postHeader',
            'class': 'form-control',
            'value': content
        }).appendTo('#header');
        $('#postHeader').focus();
        }
      });
      $('#body').click(function(){
        var content = $('#body').text();
        if(content !== '') {
        $('#body').html('');
        $('<textarea></textarea>')
        .attr({
            'rows': '10',
            'name': 'postBody',
            'id': 'postBody',
            'class': 'form-control',
        }).appendTo('#body');
        $('#postBody').val(content);
        $('#postBody').focus();
        }
      });
    </script>
    
  </body>
</html>
