<%@page contentType="text/html" pageEncoding="UTF-8"%><!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' rel='stylesheet'>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
    <title>Sign up</title>
  </head>
  <body class='container'>
    <form method='post'>
      <div class='card'>
        <div class='card-header text-center'><b>Register new user</b></div>
        <div class='card-block' style='padding:15px;'>
          <div class='form-group'>
            <label for='username'>Login:</label>
            <input type='text' class='form-control' id='username' name='username' required pattern='[A-Za-z0-9]{3,}'/>
          </div>
          <div class='form-group'>
            <label for='userpass1'>Password:</label>
            <input type='password' class='form-control' id='userpass1' name='password' required pattern='[A-Za-z0-9-=_+()$#@!~,.]{3,}'/>
          </div>
          <div class='form-group'>
            <label for='userpass2'>Confirm Password:</label>
            <input type='password' class='form-control' required id='userpass2' name='confirmpass' onchange='this.setCustomValidity(this.value === form.password.value ? "" : "Password not match");'/>
          </div>
          <div class='form-group'>
            <label for='usermail'>E-mail:</label>
            <input type='email' class='form-control' id='email' name='email'/>
          </div>
        </div>
        <div class='card-footer text-right'>
          <input type='submit' class='btn btn-success' value='Register' style='cursor:pointer;'/>
        </div>
      </div>
    </form>
  </body>
</html>