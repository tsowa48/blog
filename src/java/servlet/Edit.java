package servlet;

import blog.HibernateUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

public class Edit extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String sPostID = request.getParameter("post");
    String token = (String)request.getSession().getAttribute("token");
    
    if(!"".equals(sPostID) && !"".equals(token)) {
      Session hsSession = HibernateUtil.getSession();
      hsSession.beginTransaction();
      Object[] oPost = (Object[])hsSession.createSQLQuery("select header,body,tags,author from post where id=" + sPostID + ";").uniqueResult();
      hsSession.getTransaction().commit();
      response.setContentType("text/html;charset=UTF-8");
      request.setAttribute("header", oPost[0]);
      request.setAttribute("body", oPost[1]);
      request.setAttribute("tags", oPost[2]);
      request.setAttribute("author", oPost[3]);
      request.getRequestDispatcher("/WEB-INF/edit.jsp").forward(request, response);
    } else {
      response.sendRedirect("/main");
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String sPostID = request.getParameter("post");
    String sPostHeader = request.getParameter("postHeader");
    String sPostBody = request.getParameter("postBody");
    String token = (String)request.getSession().getAttribute("token");

    if(!"".equals(sPostHeader) && !"".equals(sPostBody) && !"".equals(sPostID) && !"".equals(token)) {
      Session hsSession = HibernateUtil.getSession();
      hsSession.beginTransaction();
      if(sPostHeader != null) {
        sPostHeader = Base64.getEncoder().encodeToString(sPostHeader.getBytes("iso-8859-1"));
        hsSession.createSQLQuery("update post set header='" + sPostHeader + "' where id=" + sPostID + ";").executeUpdate();
      }
      if(sPostBody != null) {
        sPostBody = Base64.getEncoder().encodeToString(sPostBody.getBytes("iso-8859-1"));
        hsSession.createSQLQuery("update post set body='" + sPostBody + "' where id=" + sPostID + ";").executeUpdate();
      }
      hsSession.getTransaction().commit();
    }
    response.sendRedirect("/main");
  }
}
