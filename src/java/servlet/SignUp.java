package servlet;

import blog.HibernateUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

public class SignUp extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    request.getRequestDispatcher("/WEB-INF/signup.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String sUserName = request.getParameter("username");
    String sUserPassword = request.getParameter("password");
    String sUserMail = request.getParameter("email");
    
    if(sUserName != null && sUserPassword != null) {
      try {
        Session hsSession = HibernateUtil.getSession();
        hsSession.beginTransaction();
        hsSession.createSQLQuery("insert into blogger(login,password,email) values ('" + sUserName + "', '" + sUserPassword + "', '" + (sUserMail == null ? "" : sUserMail)  + "');").executeUpdate();
        hsSession.getTransaction().commit();
        request.getSession().setAttribute("userName", sUserName);
        response.sendRedirect("/login");
      } catch(Exception ex) {
        response.sendRedirect("/sign");
      }
    } else {
      response.sendRedirect("/sign");
    }
  }
}
