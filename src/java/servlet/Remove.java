package servlet;

import blog.HibernateUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

public class Remove extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String sPostID = request.getParameter("post");
    String sCommentID = request.getParameter("comment");
    
    Session hsSession = HibernateUtil.getSession();
    hsSession.beginTransaction();
    if(sCommentID != null && !"".equals(sCommentID)) {
      hsSession.createSQLQuery("delete from comment where id=" + sCommentID + ";").executeUpdate();
      hsSession.getTransaction().commit();
    } else if(sPostID != null && !"".equals(sPostID)) {
      hsSession.createSQLQuery("delete from post where id=" + sPostID + ";").executeUpdate();
      hsSession.createSQLQuery("delete from comment where pid=" + sPostID + ";").executeUpdate();
      hsSession.getTransaction().commit();
      response.sendRedirect("/main");
    }
  }
}
