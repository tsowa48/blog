package servlet;

import blog.HibernateUtil;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

public class View extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    String token = (String)request.getSession().getAttribute("token");
    
    String sPostID = request.getParameter("post");
    String sUserName = request.getParameter("user");
    
    Session hsSession = HibernateUtil.getSession();
    hsSession.beginTransaction();
    if(sUserName != null && !"".equals(sUserName)) {
      Object[] oUser = (Object[])hsSession.createSQLQuery("select B.login, B.email, (select count(P.id) from post P where P.author=B.login) from blogger B where B.login='" + sUserName + "';").uniqueResult();
      hsSession.getTransaction().commit();
      if(oUser == null) {
        response.sendRedirect("/main");
      } else {
        request.setAttribute("login", oUser[0].toString());
        request.setAttribute("email", "".equals(oUser[1].toString()) ? "no" : oUser[1].toString());
        request.setAttribute("posts", oUser[2].toString());
        request.getRequestDispatcher("/WEB-INF/view.jsp").forward(request, response);
      }
    } else if(sPostID != null && !"".equals(sPostID)) {
      Object[] oPost = (Object[])hsSession.createSQLQuery("select P.header, P.body, P.tags, P.author, B1.login, B1.email from post P, blogger B1 where B1.token='" + token + "' and P.id=" + sPostID + ";").uniqueResult();
      List<Object[]> lstComments = (List)hsSession.createSQLQuery("select C.author, C.body, B.email, C.id from comment C, blogger B where B.login=C.author and C.pid=" + sPostID + ";").list();
      hsSession.getTransaction().commit();
      if(oPost == null) 
        response.sendRedirect("/main");
      else {
        request.setAttribute("header", oPost[0]);
        request.setAttribute("body", oPost[1]);
        request.setAttribute("tags", oPost[2]);
        request.setAttribute("author", oPost[3]);
        request.setAttribute("me", oPost[4]);
        request.setAttribute("myMail", oPost[5]);
        request.setAttribute("comments", lstComments);
        request.getRequestDispatcher("/WEB-INF/view.jsp").forward(request, response);
      }
    } else {
      hsSession.getTransaction().commit();
      response.sendRedirect("/main");
    }
  }
}
