package servlet;

import blog.HibernateUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

public class addComment extends HttpServlet {

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String sCommentText = request.getParameter("newComment").replaceAll("-", "+").replaceAll(",", "/").replaceAll("_", "=");
    String sPostID = request.getParameter("post");
    String token = (String)request.getSession().getAttribute("token");
    if(!"".equals(sCommentText) && !"".equals(token) && !"".equals(sPostID)) {
      Session hsSession = HibernateUtil.getSession();
      hsSession.beginTransaction();
      hsSession.createSQLQuery("insert into comment(author,body,pid) values ((select login from blogger where token='" + token + "'),'" + sCommentText + "'," + sPostID + ");").executeUpdate();
      hsSession.getTransaction().commit();
    }
  }
}