package servlet;

import blog.HibernateUtil;
import java.io.IOException;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

public class Login extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String token = UUID.randomUUID().toString();
    String sUserName = request.getParameter("username");
    String sUserPassword = request.getParameter("password");
    try {
      if(sUserName == null || sUserPassword == null)
        throw new Exception("no required params");
      Session hsSession = HibernateUtil.getSession();
      hsSession.beginTransaction();
      hsSession.createSQLQuery("update blogger set token='" + token + "' where login='" + sUserName + "' and password='" + sUserPassword + "';").executeUpdate();
      hsSession.getTransaction().commit();
      request.getSession(true).setAttribute("token", token);
      response.sendRedirect("/main");
    } catch(Exception ex) {
      response.setContentType("text/html;charset=UTF-8");
      request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
    }
  }
}
