package servlet;

import blog.HibernateUtil;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

public class Main extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String token = (String)request.getSession().getAttribute("token");
    if(!"".equals(token)) {
      response.setContentType("text/html;charset=UTF-8");
      Session hsSession = HibernateUtil.getSession();
      hsSession.beginTransaction();
      String sUserName = hsSession.createSQLQuery("select login from blogger where token='" + token + "';").uniqueResult().toString();
      if(!"".equals(sUserName)) {
        request.setAttribute("user", sUserName);
        List<Object[]> lstPosts = hsSession.createSQLQuery("select id,header,author from post order by id desc;").list();
        hsSession.getTransaction().commit();
        request.setAttribute("posts", lstPosts);
        request.getRequestDispatcher("/WEB-INF/main.jsp").forward(request, response);
      } else {
        hsSession.getTransaction().commit();
        response.sendRedirect("/login");
      }
    } else
      response.sendRedirect("/login");
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String token = request.getSession().getAttribute("token").toString();
    String sHeader = request.getParameter("header").replaceAll("-", "+").replaceAll(",", "/").replaceAll("_", "=");
    String sBody = request.getParameter("body").replaceAll("-", "+").replaceAll(",", "/").replaceAll("_", "=");
    String sTags = request.getParameter("tags").replaceAll("-", "+").replaceAll(",", "/").replaceAll("_", "=");
    if(!"".equals(token) || !"".equals(sHeader) || !"".equals(sBody) || !"".equals(sTags)) {
      Session hsSession = HibernateUtil.getSession();
      hsSession.beginTransaction();
      hsSession.createSQLQuery("insert into post(header,body,tags,author) values ('" + sHeader + "','" + sBody + "','" + sTags + "',(select B.login from blogger B where B.token='" + token + "'));").executeUpdate();
      hsSession.getTransaction().commit();
    }
  }
}
