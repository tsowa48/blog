package blog;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.metamodel.Metadata;
import org.hibernate.metamodel.MetadataSources;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

  private static SessionFactory sessionFactory;
  private static Session session;
    
  static {
    if(sessionFactory == null || sessionFactory.isClosed()) {
      Configuration configuration = new Configuration();
      configuration.configure();
      ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
      sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }
    if(session == null || !session.isOpen())
      session = sessionFactory.openSession();
  }
  
  /*static {
    try {
      StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
      Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
      sessionFactory = metaData.getSessionFactoryBuilder().build();
      session = sessionFactory.openSession();
    } catch (Throwable th) {
      System.err.println("Enitial SessionFactory creation failed " + th);
      throw new ExceptionInInitializerError(th);
    }
  }//*/
  public static Session getSession() {
    if(session == null || !session.isOpen())
      session = sessionFactory.openSession();
    return session;
  }
}