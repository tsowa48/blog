## Personal Blog
### Функционал: 
1. /welcome - простая страница приветствия для неавторизованных пользователей. На ней ссылки на /sign и /login 
2. /sign - регистрация пользователя. вводим логин и пароль с подтверждением. опциональное поле - email. Если регистрация прошла успешно - редиректим на /login 
3. /login - точка авторизации. вводим пару login/password, если авторизация успешна - переходим на /main 
4. /main - главная страница блога. Отображаем списком посты пользователей:
4.1. заголовок поста является ссылкой на страницу поста
4.2. имя пользователя в виде ссылки на страничку с профилем пользователя, где указано в т.ч. общее количество его постов.
4.3. есть кнопки "new post", для своих постов - "edit post", "delete post" 
5. при создании нового поста поля ввода: "заголовок", "тело поста" и "тэги"(просто строковое поле) 
6. ввод комментариев для каждого поста (при вводе комментариев поля ввода "автор", "email" и "text")
7. разделение прав: пользователь может править и удалять только свои посты, автор поста может удалять чужие комментарии. остальные пользователи могут только писать новые

SQL:
create database blog with owner blog;
create table blogger(login varchar(64) not null primary key, password varchar(64) not null, email varchar(128), token text);
create table post(header text not null, body text not null, tags text, author varchar(64) not null, id bigserial primary key not null);
create table comment(author varchar(64) not null, body text not null, pid bigint not null);